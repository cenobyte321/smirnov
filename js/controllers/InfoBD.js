/**
 * Comment
 */
var infoBDController;

function InfoBD() {
    this.conexionInst = new Conexion();
    this.bdModeloInst = new BaseDeDatos();
    infoBDController = this;
    this.selectDOMElementI = "";
    this.tablasDOMElement = "";
    this.dbNombreVar = "";
    this.tablasSeleccionadas = new Array;
}

InfoBD.prototype =
{
    mostrarBasesDeDatos : function(selectDOMElement)
    {
        this.selectDOMElement = selectDOMElement;
        infoBDController.conexionInst.fetch(this.obtenerBasesDeDatos,direcciones.listarBDs);
        
    },
    
    obtenerBasesDeDatos : function(data)
    {
        infoBDController.listaBD = data;
        var optionSelect = ''
        for (var i = 0; i < infoBDController.listaBD.length; i++) {
            optionSelect += '<option value="'+infoBDController.listaBD[i]+'">'+infoBDController.listaBD[i]+'</option>';
        }
        $('select#'+infoBDController.selectDOMElement).append(optionSelect);
    },
    
    mostrarTablas : function(divDOMElement,dbNombreParam)
    {
        this.dbNombreVar = dbNombreParam;
        infoBDController.conexionInst.put(this.obtenerTablas,direcciones.listarTablas,{
            dbNombre: dbNombreParam
        });
        this.tablasDOMElement = divDOMElement;
        
    },
    
    obtenerTablas: function(data)
    {
        var output = '<fieldset><legend>Tablas de la base de datos '+infoBDController.dbNombreVar+'</legend>'+
        '<table id="tablasSeleccionadas">';
        for(var i = 0; i < data.length; i++)
        {
            output += '<tr><td>'+data[i].tabla+'</td> <td><input type="checkbox" value="'+data[i].tabla+'" name="tablasChecked[]" /></td></tr>';
        }
        output += '</table></fieldset>';
        $('div#'+infoBDController.tablasDOMElement).append(output);
       
    },
    listarResultados : function(ulDOMElement)
    {
        var output = ''
        $.each($("input[name='tablasChecked[]']:checked"), function() {
            infoBDController.tablasSeleccionadas.push($(this).val());
            output += '<li>' + $(this).val() + '</li>';
        });
        $('ul#'+ulDOMElement).append(output);
    },
    
    generarCodigo : function()
    {
        var generarCodigo = new CodeWriter(this.dbNombreVar, this.tablasSeleccionadas);
    }
    
}