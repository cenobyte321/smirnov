var codeWriterController;

function CodeWriter(dbNombreParam, tablasParam)
{
    this.dbNombre = dbNombreParam;
    this.tablas = tablasParam;
    this.conexion = new Conexion();
    codeWriterController = this;
    this.obtenerCampos();
}

CodeWriter.prototype = 
{
    obtenerCampos : function()
    {
        codeWriterController.archivosBase();
        for (var i = 0; i < this.tablas.length; i++) {
            this.conexion.put(this.crearArchivos, direcciones.listarCampos, {
                dbNombre: this.dbNombre,
                tablaNombre: this.tablas[i]
            })
        }
    },
    archivosBase: function()
    {
        codeWriterController.crearArchivoConexion();
    },
    crearArchivos : function(dataParam)
    {
        console.log(dataParam);
        var llavePrimaria = '';
        for(var i = 0; i < dataParam.campos.length; i++)
        {
            if(dataParam.campos[i].Key == "PRI")
            {
                llavePrimaria = dataParam.campos[i].Field;
            }
        }
        codeWriterController.crearArchivoModelo(dataParam.tabla, dataParam.campos, llavePrimaria);
        codeWriterController.crearArchivoControlador(dataParam.tabla, dataParam.campos, llavePrimaria);
        codeWriterController.crearArchivosVistas(dataParam.tabla, dataParam.campos, llavePrimaria);
        
    },
    crearArchivoConexion : function()
    {
        var outputDatabasePHP = '<?php\n' + 
        '/**\n' + 
        ' * Clase database\n' + 
        ' *\n' + 
        ' * Contiene informacion sobre la conexion a la Base de Datos.\n' + 
        ' *\n' + 
        ' * @version 1.0\n' + 
        ' * @access  public\n' + 
        ' */\n' + 
        '\n' + 
        'class database\n' + 
        '{\n' + 
        '\n' + 
        '/**\n' + 
        '   \t * Variable que representa el host del servidor SQL.\n' + 
        '   \t *\n' + 
        '   \t * @var    string\n' + 
        '   \t * @access private\n' + 
        '   \t */\n' + 
        '  \t private $_dbHost = "localhost:3306";\n'+ 
        '\t \n' + 
        '  \t /**\n' + 
        '   \t * Variable que representa el nombre de la base de datos.\n' + 
        '   \t *\n' + 
        '   \t * @var    string\n' + 
        '   \t * @access private\n' + 
        '  \t  */\n' + 
        '  \t private $_dbNombre = "'+codeWriterController.dbNombre+'";\n' + 
        '\n' + 
        '  \t /**\n' + 
        '  \t  * Variable que representa el nombre del usuario para realizar la\n' + 
        '   \t * conexion a la Base de Datos\n' + 
        '  \t  *\n' + 
        '  \t  * @var    string\n' + 
        '  \t  * @access private\n' + 
        '  */\n' + 
        '  \t private $_dbUsuario = "root";\n' + 
        '\n' + 
        ' \t  /**\n' + 
        '  \t  * Variable que representa el password del usuario de la base de datos.\n' + 
        '   \t *\n' + 
        '  \t  * @var    string\n' + 
        '  \t  * @access private\n' + 
        '  \t  */\n' + 
        ' \t  private $_dbPassword = "";\n' + 
        '\n' + 
        ' \t  /**\n' + 
        '  \t  * Variable que representa el enlace de conexion a la base de datos.\n' + 
        '  \t  *\n' + 
        '   \t * @var    string\n' + 
        '   \t * @access private\n' + 
        ' \t   */\n' + 
        '  \t private $db;\n' + 
        '\n' + 
        '\n' + 
        '  \t /**\n' + 
        '   \t * Metodo que realiza una conexion con la Base de Datos.\n' + 
        '   \t *\n' + 
        '   \t * @access public\n' + 
        '   \t * @see $db\n' + 
        '   \t */\n' + 
        '  \t function conectar()\n' + 
        '  \t {\n' + 
        '   \t  \t $this->db = mysql_connect( $this->_dbHost, $this->_dbUsuario, $this->_dbPassword ) or die("No se pudo connectar a la base de datos.");\n' + 
        '    \t \t mysql_select_db($this->_dbNombre, $this->db);\n' + 
        '  \t }\n' + 
        '\n' + 
        '\n' + 
        '  \t /**\n' + 
        '  \t  * Cierra un enlace con la base de datos.\n' + 
        '   \t *\n' + 
        '  \t  * @access public\n' + 
        '   \t */\n' + 
        '  \t function cerrar()\n' + 
        '  \t {\n' + 
        '    \t \t mysql_close( $this->db );\n' + 
        '  \t }\n' + 
        '\n' + 
        '\n' + 
        '  \t /**\n' + 
        '   \t * Metodo que recibe un query, lo ejecuta con el enlace a la BD, y regresa\n' + 
        '   \t * el resultado.\n' + 
        '   \t *\n' + 
        '   \t * @access public\n' + 
        '   \t * @param string $query Instruccion SQL a ejecutar\n' + 
        '   \t * @return object\n' + 
        '   \t */\n' + 
        '  \t function ejecutarQuery( $query )\n' + 
        ' \t  {\n' + 
        '    \t \t $result2 = array();\n' + 
        '    \t \t $result = mysql_query( $query, $this->db ) or die("No se pudo ejecutar el query:".$query );\n' + 
        '   \t \t  while ( $resultT = mysql_fetch_array( $result ) )\n' + 
        '      \t \t $result2[] = $resultT;\n' + 
        '    \t \t return $result2;\n' + 
        '  \t }\n' + 
        '\n' + 
        '   \t  /**\n' + 
        '   \t * Metodo que recibe un query, lo ejecuta con el enlace a la BD   *\n' + 
        '   \t * @access public\n' + 
        '   \t * @param string $query Instruccion SQL a ejecutar\n' + 
        '   \t * @return object\n' + 
        '   \t */\n' + 
        ' \t  function ejecutarQueryWrite($query) {\n' + 
        '       \t \t $result = mysql_query( $query, $this->db ) or die("No se pudo ejecutar el query:".$query );\n' + 
        '       \t \t return $result;\n' + 
        ' \t  }\n' + 
        '}\n' + 
        '\n' + 
        '?>\n';
		
        codeWriterController.conexion.put(codeWriterController.terminado, direcciones.crearArchivo, {
            archivoNombre : 'database.php', 
            contenidos: outputDatabasePHP, 
            carpeta:'models'
        })
    },
	
    crearArchivoModelo : function(tabla, campos, priKey)
    {
        var outputDatosPHP = '<?php\n' + 
        '\n' + 
        '/**\n' + 
        ' * Clase '+tabla+'Datos\n' + 
        ' *\n' + 
        ' * Clase que representa a la tabla '+tabla+'\n' + 
        ' *\n' + 
        ' * @version 1.0\n' + 
        ' * @access public\n' + 
        ' */\n' + 
        'require_once "database.php";\n' + 
        '\n' + 
        'class '+tabla+'Datos extends database {\n' + 
        '\t \n';
				 
        for(var i = 0; i < campos.length; i++)
        {
            outputDatosPHP +=  '    \t /**\n' + 
            '\t* Variable que contiene el '+campos[i].Field+' de '+tabla+'\n' + 
            '     \t */\n'+
            '   \t  private $_'+campos[i].Field+';\n';

        }
		
        outputDatosPHP +=  '\n' + 
        '    \t /**\n' + 
        '     \t * Constructor.\n' + 
        '     \t *\n' + 
        '     \t */\n' + 
        '    \t function '+tabla+'Datos() {\n' + 
        '        \n' + 
        '    \t }\n' + 
        '\n'+
        '    \t /**\n' + 
        '    \t  * Funcion para obtener un '+tabla+' por su id\n' + 
        '     \t * @param $numero'+tabla+' El id del '+tabla+'\n' + 
        '     \t * @return void\n' + 
        '     \t */\n' + 
        '    \t function get'+tabla+'ById($numero'+tabla+') {\n' + 
        '        \t \t $this->id'+tabla+'($numero'+tabla+');\n' + 
        '    \t }\n' + 
        '\n' + 
        '     \t /**\n' + 
        '     \t * Funcion para obtener todos los '+tabla+'es\n' + 
        '     \t * @return array\n' + 
        '     \t */\n' + 
        '    \t function getAll'+tabla+'() {\n' + 
        '        \t \t return $this->all'+tabla+'();\n' + 
        '    \t }\n' + 
        '\n' + 
        '     \t /**\n' + 
        '     \t * Funcion para agregar un '+tabla+' a la base de datos\n' + 
        '     \t * @return bool\n' + 
        '     \t */\n' + 
        '    \t function agregar'+tabla+'() {\n' + 
        '        \t \t return $this->insert'+tabla+'();\n' + 
        '    \t }\n' + 

        '     \t /**\n' + 
        '     \t * Funcion para eliminar un '+tabla+' de la base de datos\n' + 
        '     \t * @param $numero'+tabla+' El id del '+tabla+'\n' + 
        '     \t * @return bool\n' + 
        '     \t */\n' + 
        '    \t function eliminar'+tabla+'($numero'+tabla+') {\n' + 
        '        \t \t return $this->delete'+tabla+'($numero'+tabla+');\n' + 
        '    \t }\n' + 
        '\n' + 
        '     \t /**\n' + 
        '     \t * Funcion para actualizar una '+tabla+' dentro de la base de datos\n' + 
        '     \t * @param int $numero'+tabla+' El id del '+tabla+'\n' + 
        '     \t * @return bool\n' + 
        '     \t */\n' + 
        '     \t function actualiza'+tabla+'($numero'+tabla+') {\n' + 
        '        \t \t return $this->update'+tabla+'($numero'+tabla+');\n' + 
        '     \t }\n';
        for(i = 0; i < campos.length; i++)
        {
            outputDatosPHP +=   '\n' + 
            '    \t /**\n' + 
            '     \t * Metodo set de la propiedad _'+campos[i].Field+'\n' + 
            '     \t */\n' + 
            '    \t function set'+campos[i].Field+'($'+campos[i].Field+') {\n' + 
            '       \t \t  $this->_'+campos[i].Field+' = $'+campos[i].Field+';\n' + 
            '    \t }\n' + 
            '\n' + 
            '    \t /**\n' + 
            '     \t * Metodo get de la propiedad _'+campos[i].Field+'\n' + 
            '     \t */\n' + 
            '    \t function get'+campos[i].Field+'() {\n' + 
            '        \t \t return $this->_'+campos[i].Field+';\n' + 
            '    \t }\n' + 
            '\n';
        }
        outputDatosPHP += '    \t /**\n' + 
        '     \t * Manda ejecutar el Select de los datos de la tabla '+tabla+', seleccionando un solo elemento y asignando los campos a los atributos.\n' + 
        '     \t * @access private\n' + 
        '     \t * @param int $numero'+tabla+' El id del registro\n' + 
        '     \t * @return void\n' + 
        '     \t */\n' + 
        '    \t private function id'+tabla+'($numero'+tabla+') {\n' + 
        '        \t \t parent::conectar();\n' + 
        '        \t $query = "SELECT * FROM '+tabla+' WHERE '+priKey+' = " . $numero'+tabla+'. "";\n' + 
        '        \t \t $result = parent::ejecutarQuery($query);\n';
        for (i = 0; i < campos.length; i++) 
        {
            outputDatosPHP +=  '        \t \t $this->_'+campos[i].Field+' = $result[0][\''+campos[i].Field+'\'];\n';
        }

        outputDatosPHP += '        \t \t parent::cerrar();\n' + 
        '    \t }\n' + 
        '\n';
        
        outputDatosPHP +=  '    \t /**\n' + 
        '     \t * Manda ejecutar el Select de los datos de la tabla '+tabla+', seleccionando todos los registros.\n' + 
        '     \t * @access private\n' + 
        '     \t * @return resource\n' + 
        '     \t */\n' + 
        '    \t private function all'+tabla+'() {\n' + 
        '        \t \t parent::conectar();\n' + 
        '        \t \t $query = "SELECT * FROM '+tabla+'";\n' + 
        '        \t \t $result = parent::ejecutarQuery($query);\n' + 
        '        \t \t parent::cerrar();\n' + 
        '        \t \t return $result;\n' + 
        '    \t }\n'+
        '\n' + 
        '     \t /**\n' + 
        '     \t * Inserta un '+tabla+' a la base de datos tomando los atributos de esta clase\n' + 
        '     \t * @access private\n' + 
        '     \t * @return bool\n' + 
        '     \t */\n' + 
        '    \t private function insert'+tabla+'() {\n' + 
        '        \t \t parent::conectar();\n';

 
        var outputTempCampos = '';
        var outputTempAsignaciones = '';
        for (i = 0; i < campos.length; i++) 
        {
            if(campos[i].Field != priKey)
            {
                outputTempCampos += ''+campos[i].Field + ' = \'%s\'';  
                if(i < campos.length - 1)
                    outputTempCampos += ',';
            }
        }
        for (i = 0; i < campos.length; i++) 
        {
            if(campos[i].Field != priKey)
            {
                outputTempAsignaciones += 'strip_tags(mysql_real_escape_string($this->_'+campos[i].Field+'))';
                if(i < campos.length - 1)
                    outputTempAsignaciones += ',';
            }
        }
        
        outputDatosPHP += '        \t \t $query = sprintf("INSERT INTO '+tabla+' SET '+outputTempCampos+'",'+outputTempAsignaciones+');\n'+
        '       \t \t  $result = parent::ejecutarQueryWrite($query);\n' + 
        '        \t \t parent::cerrar();\n' + 
        '        \t \t if (!$result) {\n' + 
        '            \t \t \t return false;\n' + 
        '        \t \t } else {\n' + 
        '            \t \t \t return true;\n' + 
        '        \t \t }\n' + 
        '    \t }\n';

        outputDatosPHP += '    \t  /**\n' + 
        '     \t * Elimina un '+tabla+' de la base de datos\n' + 
        '     \t * @access private\n' + 
        '     \t * @param int $numero'+tabla+' El id del '+tabla+' a eliminar\n' + 
        '     \t * @return bool\n' + 
        '     \t */\n' + 
        '    \t private function delete'+tabla+'($numero'+tabla+') {\n' + 
        '        \t \t parent::conectar();\n' + 
        '        \t \t $query = sprintf("DELETE FROM '+tabla+' WHERE '+priKey+' = %d", $numero'+tabla+');\n' + 
        '        \t \t $result = parent::ejecutarQueryWrite($query);\n' + 
        '        \t \t parent::cerrar();\n' + 
        '        \t \t if (!$result) {\n' + 
        '            \t \t \t return false;\n' + 
        '        \t \t } else {\n' + 
        '            \t \t \t return true;\n' + 
        '        \t \t }\n' + 
        '     \t }\n' + 
        '\n';
    
        outputDatosPHP +='     \t /**\n' + 
        '     \t * Actualiza los campos de un '+tabla+' dentro de la base de datos\n' + 
        '     \t * @access private\n' + 
        '     \t * @param int $numero'+tabla+' El id del '+tabla+' a actualizar\n' + 
        '     \t * @return bool\n' + 
        '     \t */\n' + 
        '    \t private function update'+tabla+'($numero'+tabla+') {\n' + 
        '        \t \t parent::conectar();\n' + 
        '        \t \t $query = sprintf("UPDATE '+tabla+' SET '+outputTempCampos+' WHERE '+priKey+' = %d",'+outputTempAsignaciones+', $numero'+tabla+');\n' + 
        '        \t \t $result = parent::ejecutarQueryWrite($query);\n' + 
        '        \t \t parent::cerrar();\n' + 
        '        \t \t if (!$result) {\n' + 
        '            \t \t \t return false;\n' + 
        '        \t \t } else {\n' + 
        '            \t \t \t return true;\n' + 
        '        \t \t }\n' + 
        '    \t }\n';

        outputDatosPHP +=  '\n' + 
        '}\n' + 
        '\n' + 
        '?>\n';
        codeWriterController.conexion.put(codeWriterController.terminado, direcciones.crearArchivo, {
            archivoNombre : tabla+'Datos.php', 
            contenidos: outputDatosPHP, 
            carpeta:'models'
        });		
    },
    crearArchivoControlador : function(tabla,campos,priKey)
    {
        var outputLogicaPHP = '<?php\n' + 
        '\n' + 
        'require_once "models/'+tabla+'Datos.php";\n' + 
        '\n' + 
        'class '+tabla+'Logica {\n' + 
        '\n' + 
        '\n' + 
        '\tfunction '+tabla+'Logica() {\n' + 
        '\n' + 
        '\t}\n' + 
        '\n' + 
        '\t/**\n' + 
        '\t* Colecciona todos los registros de '+tabla+' de la base de datos y muestra la vista con el listado de '+tabla+'\n' + 
        '\t* @access public\n' + 
        '\t* @param string $notice Mensaje opcional de advertencia a mostrar\n' + 
        '\t* @return void\n' + 
        '\t*/\n' + 
        '\tfunction listar'+tabla+'($notice = NULL) {\n' + 
        '\t\t$'+tabla+'Dat = new '+tabla+'Datos();\n' + 
        '\t\t$'+tabla+' =  $'+tabla+'Dat->getAll'+tabla+'();\n' + 
        '\t\t$'+tabla+'s = array();\n' + 
        '\t\tforeach ($'+tabla+' as $elemento) {\n' + 
        '\t\t\t$temp = new '+tabla+'Datos();\n' + 
        '\t\t\t$temp->get'+tabla+'ById($elemento[\''+priKey+'\']);\n' + 
        '\t\t\tarray_push($'+tabla+'s, $temp);\n' + 
        '\t\t}\n' + 
        '\t\tinclude "views/r'+tabla+'Vista.php";\n' + 
        '\t}\n' + 
        '\n' + 
        '\t/**\n' + 
        '\t* Muestra una forma para insertar un '+tabla+'\n' + 
        '\t* @access public\n' + 
        '\t* @return void\n' + 
        '\t*/\n' + 
        '\tfunction agregar() {\n' + 
        '\t\tinclude "views/c'+tabla+'Vista.php";\n' + 
        '\t}\n' + 
        '\n' + 
        '\t/**\n' + 
        '\t* Inserta un '+tabla+' enviado por una forma. Al finalizar lista los registros o regresa un mensaje de error y muestra la forma para insertar \n' + 
        '\t* @access public\n' + 
        '\t* @return void\n' + 
        '\t*/\n' + 
        '\tfunction crear'+tabla+'() {\n'+
        '\t\t$validacion = true;\n'+
        '\t\t$'+tabla+'Dat = new '+tabla+'Datos();\n';
        
        for(var i = 0; i < campos.length; i++)
        {
            outputLogicaPHP +=  '\t\t$'+campos[i].Field+' = $_REQUEST[\''+campos[i].Field+'\'];\n'+
            '\t\t$'+tabla+'Dat->set'+campos[i].Field+'($'+campos[i].Field+');\n';
            if(campos[i].Field != priKey)
            {
                var tipo = campos[i].Type.substr(0, campos[i].Type.indexOf('('));
                switch(tipo)
                {
                    case 'int':
                        outputLogicaPHP +=  '\t\t$validacion = (filter_var($'+campos[i].Field+', FILTER_VALIDATE_INT)) ? true: false;\n';
                        break;
                
                    case 'double':
                        outputLogicaPHP +=  '\t\t$validacion = (filter_var($'+campos[i].Field+', FILTER_VALIDATE_DOUBLE)) ? true: false;\n';
                    
                        break;
                    
                    case 'tinyint':
                        outputLogicaPHP +=  '\t\t$validacion = (filter_var($'+campos[i].Field+', FILTER_VALIDATE_BOOLEAN)) ? true: false;\n';
                        break;
                }
            }
            
        }
           
        outputLogicaPHP += '\t\tif($validacion){'+    
        '\t\t\tif(!$'+tabla+'Dat->agregar'+tabla+'()) {\n' + 
        '\t\t\t\t$notice = "Error al agregar el nuevo registro";\n' + 
        '\t\t\t\tinclude "views/c'+tabla+'Vista.php";\n' + 
        '\t\t\t\treturn;\n' + 
        '\t\t\t}\n' +
        '\n' + 
        '\t\t\t$notice = "Registro agregado correctamente";\n' + 
        '\t\t}else{\n'+
        '\t\t\t$notice = "Error de validacion";\n'+
        '\t\t}\n'+
        '\t\t$this->listar'+tabla+'($notice);\n' + 
        '\n' + 
        '\t}\n' + 
        '\n' + 
        '\t/**\n' + 
        '\t* Elimina una serie de '+tabla+' seleccionados por una coleccion de checkboxes. Al final carga la vista que enlista todos los '+tabla+'\n' + 
        '\t* Si no se selecciona alguno se muestra un mensaje de error\n' + 
        '\t* @access public\n' + 
        '\t* @return void\n' + 
        '\t*/\n' + 
        '\tfunction eliminarseleccionados() {\n' + 
        '\n' + 
        '\t\tif (empty ($_REQUEST[\'selected\'])) {\n' + 
        '\t\t\t$notice = "No se seleccionaron '+tabla+' a borrar";\n' + 
        '\t\t\t$this->listar'+tabla+'($notice);\n' + 
        '\t\t\treturn;\n' + 
        '\t\t}\n' + 
        '\n' + 
        '\t\t$'+tabla+'Dat = new '+tabla+'Datos();\n' + 
        '\t\t$exito = true;\n' + 
        '\n' + 
        '\n' + 
        '\t\tforeach ($_REQUEST[\'selected\'] as $id) {\n' + 
        '\t\t\tif(!$'+tabla+'Dat->eliminar'+tabla+'($id)) {\n' + 
        '\t\t\t\t$exito = false;\n' + 
        '\t\t\t}\n' + 
        '\t\t}\n' + 
        '\t\t$notice = ($exito == true) ? "Registros eliminados!": "Hubo un error al eliminar registros";\n' + 
        '\t\t$this->listar'+tabla+'($notice);\n' + 
        '\n' + 
        '\t}\n' + 
        '\n' + 
        '\t/**\n' + 
        '\t* Muestra una forma con los '+tabla+' seleccionados (por una coleccion de checkboxes) a ser actualizados\n' + 
        '\t* Si no se selecciona '+tabla+' alguno se muestra un mensaje de error y se enlistan los '+tabla+'\n' + 
        '\t* @access public\n' + 
        '\t* @return void\n' + 
        '\t*/\n' + 
        '\tfunction actualizarseleccionados() {\n' + 
        '\t\tif (empty ($_REQUEST[\'selected\'])) {\n' + 
        '\t\t\t$notice = "No se seleccionaron '+tabla+'es a actualizar";\n' + 
        '\t\t\t$this->listar'+tabla+'($notice);\n' + 
        '\t\t\treturn;\n' + 
        '\t\t}\n' + 
        '\t\t$'+tabla+'s = array();\n' + 
        '\t\tforeach ($_REQUEST[\'selected\'] as $elemento) {\n' + 
        '\t\t\t$temp = new '+tabla+'Datos();\n' + 
        '\t\t\t$temp->get'+tabla+'ById($elemento);\n' + 
        '\t\t\tarray_push($'+tabla+'s, $temp);\n' + 
        '\t\t}\n' + 
        '\n' + 
        '\t\tinclude "views/u'+tabla+'Vista.php";\n' + 
        '\n' + 
        '\n' + 
        '\t}\n' + 
        '\n' + 
        '\t/**\n' + 
        '\t* Actualiza un arreglo de '+tabla+' de acuerdo con los datos enviados por una forma\n' + 
        '\t* Se regresa de nuevo al listado de '+tabla+' mostrando un mensaje de exito o de error\n' + 
        '\t* @access public\n' + 
        '\t* @return void\n' + 
        '\t*/\n' + 
        '\tfunction actualizar'+tabla+'() {\n' + 
        '\t\t$exito = true;\n' + 
        '\t\t$'+tabla+'Dat = new '+tabla+'Datos();\n' + 
        '\t\tfor($i = 0; $i < count($_POST[\''+priKey+'\']); $i++) {\n';
        for(i = 0; i < campos.length; i++)
        {
            outputLogicaPHP += '\t\t\t$'+tabla+'Dat->set'+campos[i].Field+'($_POST[\''+campos[i].Field+'\'][$i]);\n';
            
        }
        outputLogicaPHP += '\t\t\tif(!$'+tabla+'Dat->actualiza'+tabla+'($_POST[\''+priKey+'\'][$i])){\n' + 
        '$exito = false;\n' + 
        '\t\t\t}\n' + 
        '\t\t}\n' + 
        '\n' + 
        '\t\t$notice = ($exito == true) ? "Registros actualizados!": "Hubo un error al eliminar registros";\n' + 
        '\t\t$this->listar'+tabla+'($notice);\n' + 
        '\t}\n' + 
        '}\n' + 
        '?>\n';
        codeWriterController.conexion.put(codeWriterController.terminado, direcciones.crearArchivo, {
            archivoNombre : tabla+'Logica.php', 
            contenidos: outputLogicaPHP, 
            carpeta:'controllers'
        });
    },
    
    crearArchivosVistas : function(tabla,campos,priKey)
    {
        var outputVistaCreate  = '<h2 class="title">\n' + 
        '\tAgregar '+tabla+'\n' + 
        '</h2>\n' + 
        '<div class="content">\n' + 
        '\t<div style="color:red;">\n' + 
        '\t\t<strong><?php if(isset($notice)) echo $notice;  ?></strong>\n' + 
        '\t</div>\n' + 
        '\t<form action="index.php" method="post">\n' + 
        '\t\t<input type="hidden" name="p" value="'+tabla+'" />\n';
        for(var i = 0; i < campos.length; i ++)
        {
            if(campos[i].Field != priKey)
                outputVistaCreate += '\t\t<p><label>'+campos[i].Field+': </label><input type="text" name="'+campos[i].Field+'" value="" /></p>\n'; 

        }
        outputVistaCreate += '\t\t<input type="submit" value="Crear '+tabla+'" name="a" />\n' + 
        '\t</form>\n' + 
        '</div>\n';
    
        codeWriterController.conexion.put(codeWriterController.terminado, direcciones.crearArchivo, {
            archivoNombre : 'c'+tabla+'Vista.php', 
            contenidos: outputVistaCreate, 
            carpeta:'views'
        });
        
        var outputVistaRead = '<h2 class="title">\n' + 
        '\t'+tabla+'\n' + 
        '</h2>\n' + 
        '<div class="content">\n' + 
        '\t<h3>Listado de '+tabla+'</h3>\n' + 
        '    <div style="color:red;">\n' + 
        '\t\t<strong><?php if(isset($notice)) echo $notice;  ?></strong>\n' + 
        '\t</div>\n' + 
        '\t<form action="index.php" method="post">\n' + 
        '\t\t<input type="hidden" name="p" value="'+tabla+'" />\n' + 
        '\t\t<table border="1" style="font-size: 12px">\n' + 
        '\t\t\t<tr>\n' + 
        '\t\t\t\t<th>\n' + 
        '\t\t\t\t\t&nbsp;\n' + 
        '\t\t\t\t</th>\n';
        for( i = 0; i < campos.length;i++)
        {
            outputVistaRead += '\t\t\t\t<th>\n' + 
            '\t\t\t\t\t'+campos[i].Field+'\n' + 
            '\t\t\t\t</th>\n'; 
        }
        outputVistaRead += '\t\t\t</tr>\n' + 
        '\t\t\t<?php\n' + 
        '\t\t\t\tforeach ($'+tabla+'s as $elemento) {\n' + 
        '\t\t\t\t\techo \'<tr>\';\n' + 
        '\t\t\t\t\techo \'<td><input type="checkbox" name="selected[]" value="\'.$elemento->get'+priKey+'().\'" />\'.\'</td>\';\n'; 
        for( i = 0; i < campos.length;i++)
        {
            outputVistaRead += '\t\t\t\t\techo \'<td>\'.$elemento->get'+campos[i].Field+'().\'</td>\';\n';
        }

        outputVistaRead += '\t\t\t\t\techo \'</tr>\';\n' + 
        '\t\t\t\t}\n' + 
        '\t\t\t\t?>\n' + 
        '\t\t</table>\n' + 
        '\t\t<br />\n' + 
        '\t\t<fieldset style="width:350px">\n' + 
        '\t\t\t<legend style="font-size:13px;">Operaciones</legend>\n' + 
        '\t\t\t<input type="submit" value="Agregar" name="a"  />\n' + 
        '\t\t\t<input type="submit" value="Eliminar seleccionados" name="a" />\n' + 
        '\t\t\t<input type="submit" value="Actualizar seleccionados" name="a" />\n' + 
        '\t\t</fieldset>\n' + 
        '\t</form>\n' + 
        '</div>\n'; 
        
        codeWriterController.conexion.put(codeWriterController.terminado, direcciones.crearArchivo, {
            archivoNombre : 'r'+tabla+'Vista.php', 
            contenidos: outputVistaRead, 
            carpeta:'views'
        });
        
        var outputVistaUpdate = '<div class="title">\n' + 
        '\t<h2>\n' + 
        '\t\tActualizar '+tabla+'\n' + 
        '\t</h2>\n' + 
        '</div>\n' + 
        '<div class="content">\n' + 
        '\t<div style="color:red;">\n' + 
        '\t\t<strong><?php if(isset($notice)) echo $notice;  ?></strong>\n' + 
        '\t</div>\n' + 
        '\t<form action="index.php" method="post">\n' + 
        '\t\t<input type="hidden" name="p" value="'+tabla+'" />\n' + 
        '\t\t<h3>Elementos a actualizar</h3>\n' + 
        '\t\t<?php\n' + 
        '\t\t\tforeach ($'+tabla+'s as $value) {\n' + 
        '\t\t\t\techo \'<input type="hidden" name="'+priKey+'[]" value="\' . $value->get'+priKey+'() . \'" />\';\n';
        for(i = 0; i < campos.length; i++)
        {
            if(campos[i].Field != priKey)
                outputVistaUpdate += '\t\t\t\techo \'<p><label>'+campos[i].Field+': </label><input type="text" name="'+campos[i].Field+'[]" value="\' . $value->get'+campos[i].Field+'() . \'" /> </p>\';\n';
        }
        outputVistaUpdate += '\t\t\t}\n' + 
        '\t\t?>\n' + 
        '\t\t<input type="submit" value="Actualizar '+tabla+'" name="a" />\n' + 
        '\t</form>\n' + 
        '</div> ';
        codeWriterController.conexion.put(codeWriterController.terminado, direcciones.crearArchivo, {
            archivoNombre : 'u'+tabla+'Vista.php', 
            contenidos: outputVistaUpdate, 
            carpeta:'views'
        });
        
        var outputIndexPHP = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\n' + 
        '<html>\n' + 
        '\t<head>\n' + 
        '\t\t<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n' + 
        '\t\t<title>Generado por Smirnov</title>\n' + 
        '\t</head>\n' + 
        '\t<body>\n' + 
        '\t\t<div id="header">\n' + 
        '\t\t\t<h1>Mi aplicaci&oacute;n</h1>\n' + 
        '\t\t\t<p>Secciones</p>\n' + 
        '\t\t\t<ul>\n';
        for(i = 0; i < codeWriterController.tablas.length; i++)
        {
            outputIndexPHP += '\t\t\t\t<li><a href="?p='+codeWriterController.tablas[i]+'&a=listar'+codeWriterController.tablas[i]+'">'+codeWriterController.tablas[i]+'</a></li>\n';
        }
        outputIndexPHP += '\t\t\t</ul>\n' + 
        '\t\t</div>\n' + 
        '\t\t<div id="container">\n' + 
        '\t\t<?php\n' + 
        '\t\t\tif (!isset($_REQUEST[\'p\']) || !isset($_REQUEST[\'a\'])) {\n' + 
        '\t\t\t\techo \'\';\n' + 
        '\t\t\t} else {\n' + 
        '\t\t\t\trequire \'controllers/\'.$_REQUEST[\'p\'].\'Logica.php\';\n' + 
        '\t\t\t\t$clase = $_REQUEST[\'p\']."Logica";\n' + 
        '\t\t\t\t$controlador = new $clase();\n' + 
        '\t\t\t\t$accion = strtolower(str_replace(" ", "", $_REQUEST[\'a\']));\n' + 
        '\t\t\t\t$controlador->$accion();\n' + 
        '\t\t\t}\n' + 
        '\t\t\t?>\n' + 
        '\t\t</div>\n' + 
        '\t\t\n' + 
        '\t</body>\n' + 
        '</html>\n';

        codeWriterController.conexion.put(codeWriterController.terminado, direcciones.crearArchivo, {
            archivoNombre : 'index.php', 
            contenidos: outputIndexPHP, 
            carpeta:'/'
        });
    },
    terminado : function()
    {
        
    }
}