$(document).ready
    (
        function()
        {
            $('div#paso2').hide();
            $('div#paso3').hide();

            var infoBDController = new InfoBD();
            infoBDController.mostrarBasesDeDatos('basesDeDatos');

            $('span#terminaP1').click
            (
                function()
                {
                    $('div#paso1').hide(500);
                    infoBDController.mostrarTablas('tablas',$('#basesDeDatos option:selected').val());
                    $('div#paso2').show(500);
                }
                )
            ;

            $('span#atrasP2').click(function()
            {
                $('div#paso2').hide(500);
                $('div#tablas').empty();
                $('div#paso1').show(500);
            }
            );

            $('span#terminaP2').click(function(){
                $('div#paso2').hide(500);
                $('span#dbNombre').html(infoBDController.dbNombreVar);
                infoBDController.listarResultados('listaTablas');
                $('div#paso3').show(500);
            })

            $('span#atrasP3').click(function()
            {
                $('div#paso3').hide(500);
                $('div#paso2').show(500);
            }
            );  

            $('span#terminaP3').click(function()
            {
                //$('div#paso3').hide(500);
                infoBDController.generarCodigo();
            });

 
        })