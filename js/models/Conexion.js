function Conexion()
{
    
}

Conexion.prototype = 
{
    fetch: function(callbackFunction, url)
    {
        try
        {
            $.ajax
            ({
                type: "get",
                dataType: "json",
                url: url,
                success: function(data) 
                {
                    callbackFunction(data);
                }
            });
        }
        catch(ex)
        {
            alert(ex.description);
        }
    },

    put : function(callbackFunction, url,data)
    {
        $.ajax
        ({
            type: "post",
            data: data,
            dataType: "json",
            url: url,
            success: function(data) 
            {
                callbackFunction(data);
            },
            error: function(e)
            {
                console.log(e);
            }
        });
    }
};