<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Generado por Smirnov</title>
	</head>
	<body>
		<div id="header">
			<h1>Mi aplicaci&oacute;n</h1>
			<p>Secciones</p>
			<ul>
				<li><a href="?p=aplicaciones&a=listaraplicaciones">aplicaciones</a></li>
				<li><a href="?p=permisos&a=listarpermisos">permisos</a></li>
				<li><a href="?p=usuarios&a=listarusuarios">usuarios</a></li>
			</ul>
		</div>
		<div id="container">
		<?php
			if (!isset($_REQUEST['p']) || !isset($_REQUEST['a'])) {
				echo '';
			} else {
				require 'controllers/'.$_REQUEST['p'].'Logica.php';
				$clase = $_REQUEST['p']."Logica";
				$controlador = new $clase();
				$accion = strtolower(str_replace(" ", "", $_REQUEST['a']));
				$controlador->$accion();
			}
			?>
		</div>
		
	</body>
</html>
