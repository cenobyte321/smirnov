<?php
/**
 * Clase database
 *
 * Contiene informacion sobre la conexion a la Base de Datos.
 *
 * @version 1.0
 * @access  public
 */

class database
{

/**
   	 * Variable que representa el host del servidor SQL.
   	 *
   	 * @var    string
   	 * @access private
   	 */
  	 private $_dbHost = "localhost:3306";
	 
  	 /**
   	 * Variable que representa el nombre de la base de datos.
   	 *
   	 * @var    string
   	 * @access private
  	  */
  	 private $_dbNombre = "aplicaciones";

  	 /**
  	  * Variable que representa el nombre del usuario para realizar la
   	 * conexion a la Base de Datos
  	  *
  	  * @var    string
  	  * @access private
  */
  	 private $_dbUsuario = "root";

 	  /**
  	  * Variable que representa el password del usuario de la base de datos.
   	 *
  	  * @var    string
  	  * @access private
  	  */
 	  private $_dbPassword = "";

 	  /**
  	  * Variable que representa el enlace de conexion a la base de datos.
  	  *
   	 * @var    string
   	 * @access private
 	   */
  	 private $db;


  	 /**
   	 * Metodo que realiza una conexion con la Base de Datos.
   	 *
   	 * @access public
   	 * @see $db
   	 */
  	 function conectar()
  	 {
   	  	 $this->db = mysql_connect( $this->_dbHost, $this->_dbUsuario, $this->_dbPassword ) or die("No se pudo connectar a la base de datos.");
    	 	 mysql_select_db($this->_dbNombre, $this->db);
  	 }


  	 /**
  	  * Cierra un enlace con la base de datos.
   	 *
  	  * @access public
   	 */
  	 function cerrar()
  	 {
    	 	 mysql_close( $this->db );
  	 }


  	 /**
   	 * Metodo que recibe un query, lo ejecuta con el enlace a la BD, y regresa
   	 * el resultado.
   	 *
   	 * @access public
   	 * @param string $query Instruccion SQL a ejecutar
   	 * @return object
   	 */
  	 function ejecutarQuery( $query )
 	  {
    	 	 $result2 = array();
    	 	 $result = mysql_query( $query, $this->db ) or die("No se pudo ejecutar el query:".$query );
   	 	  while ( $resultT = mysql_fetch_array( $result ) )
      	 	 $result2[] = $resultT;
    	 	 return $result2;
  	 }

   	  /**
   	 * Metodo que recibe un query, lo ejecuta con el enlace a la BD   *
   	 * @access public
   	 * @param string $query Instruccion SQL a ejecutar
   	 * @return object
   	 */
 	  function ejecutarQueryWrite($query) {
       	 	 $result = mysql_query( $query, $this->db ) or die("No se pudo ejecutar el query:".$query );
       	 	 return $result;
 	  }
}

?>
