<?php

/**
 * Clase usuariosDatos
 *
 * Clase que representa a la tabla usuarios
 *
 * @version 1.0
 * @access public
 */
require_once "database.php";

class usuariosDatos extends database {
	 
    	 /**
	* Variable que contiene el id de usuarios
     	 */
   	  private $_id;
    	 /**
	* Variable que contiene el username de usuarios
     	 */
   	  private $_username;
    	 /**
	* Variable que contiene el fecha_creacion de usuarios
     	 */
   	  private $_fecha_creacion;
    	 /**
	* Variable que contiene el nombre de usuarios
     	 */
   	  private $_nombre;
    	 /**
	* Variable que contiene el apellido de usuarios
     	 */
   	  private $_apellido;
    	 /**
	* Variable que contiene el activo de usuarios
     	 */
   	  private $_activo;
    	 /**
	* Variable que contiene el password de usuarios
     	 */
   	  private $_password;

    	 /**
     	 * Constructor.
     	 *
     	 */
    	 function usuariosDatos() {
        
    	 }

    	 /**
    	  * Funcion para obtener un usuarios por su id
     	 * @param $numerousuarios El id del usuarios
     	 * @return void
     	 */
    	 function getusuariosById($numerousuarios) {
        	 	 $this->idusuarios($numerousuarios);
    	 }

     	 /**
     	 * Funcion para obtener todos los usuarioses
     	 * @return array
     	 */
    	 function getAllusuarios() {
        	 	 return $this->allusuarios();
    	 }

     	 /**
     	 * Funcion para agregar un usuarios a la base de datos
     	 * @return bool
     	 */
    	 function agregarusuarios() {
        	 	 return $this->insertusuarios();
    	 }
     	 /**
     	 * Funcion para eliminar un usuarios de la base de datos
     	 * @param $numerousuarios El id del usuarios
     	 * @return bool
     	 */
    	 function eliminarusuarios($numerousuarios) {
        	 	 return $this->deleteusuarios($numerousuarios);
    	 }

     	 /**
     	 * Funcion para actualizar una usuarios dentro de la base de datos
     	 * @param int $numerousuarios El id del usuarios
     	 * @return bool
     	 */
     	 function actualizausuarios($numerousuarios) {
        	 	 return $this->updateusuarios($numerousuarios);
     	 }

    	 /**
     	 * Metodo set de la propiedad _id
     	 */
    	 function setid($id) {
       	 	  $this->_id = $id;
    	 }

    	 /**
     	 * Metodo get de la propiedad _id
     	 */
    	 function getid() {
        	 	 return $this->_id;
    	 }


    	 /**
     	 * Metodo set de la propiedad _username
     	 */
    	 function setusername($username) {
       	 	  $this->_username = $username;
    	 }

    	 /**
     	 * Metodo get de la propiedad _username
     	 */
    	 function getusername() {
        	 	 return $this->_username;
    	 }


    	 /**
     	 * Metodo set de la propiedad _fecha_creacion
     	 */
    	 function setfecha_creacion($fecha_creacion) {
       	 	  $this->_fecha_creacion = $fecha_creacion;
    	 }

    	 /**
     	 * Metodo get de la propiedad _fecha_creacion
     	 */
    	 function getfecha_creacion() {
        	 	 return $this->_fecha_creacion;
    	 }


    	 /**
     	 * Metodo set de la propiedad _nombre
     	 */
    	 function setnombre($nombre) {
       	 	  $this->_nombre = $nombre;
    	 }

    	 /**
     	 * Metodo get de la propiedad _nombre
     	 */
    	 function getnombre() {
        	 	 return $this->_nombre;
    	 }


    	 /**
     	 * Metodo set de la propiedad _apellido
     	 */
    	 function setapellido($apellido) {
       	 	  $this->_apellido = $apellido;
    	 }

    	 /**
     	 * Metodo get de la propiedad _apellido
     	 */
    	 function getapellido() {
        	 	 return $this->_apellido;
    	 }


    	 /**
     	 * Metodo set de la propiedad _activo
     	 */
    	 function setactivo($activo) {
       	 	  $this->_activo = $activo;
    	 }

    	 /**
     	 * Metodo get de la propiedad _activo
     	 */
    	 function getactivo() {
        	 	 return $this->_activo;
    	 }


    	 /**
     	 * Metodo set de la propiedad _password
     	 */
    	 function setpassword($password) {
       	 	  $this->_password = $password;
    	 }

    	 /**
     	 * Metodo get de la propiedad _password
     	 */
    	 function getpassword() {
        	 	 return $this->_password;
    	 }

    	 /**
     	 * Manda ejecutar el Select de los datos de la tabla usuarios, seleccionando un solo elemento y asignando los campos a los atributos.
     	 * @access private
     	 * @param int $numerousuarios El id del registro
     	 * @return void
     	 */
    	 private function idusuarios($numerousuarios) {
        	 	 parent::conectar();
        	 $query = "SELECT * FROM usuarios WHERE id = " . $numerousuarios. "";
        	 	 $result = parent::ejecutarQuery($query);
        	 	 $this->_id = $result[0]['id'];
        	 	 $this->_username = $result[0]['username'];
        	 	 $this->_fecha_creacion = $result[0]['fecha_creacion'];
        	 	 $this->_nombre = $result[0]['nombre'];
        	 	 $this->_apellido = $result[0]['apellido'];
        	 	 $this->_activo = $result[0]['activo'];
        	 	 $this->_password = $result[0]['password'];
        	 	 parent::cerrar();
    	 }

    	 /**
     	 * Manda ejecutar el Select de los datos de la tabla usuarios, seleccionando todos los registros.
     	 * @access private
     	 * @return resource
     	 */
    	 private function allusuarios() {
        	 	 parent::conectar();
        	 	 $query = "SELECT * FROM usuarios";
        	 	 $result = parent::ejecutarQuery($query);
        	 	 parent::cerrar();
        	 	 return $result;
    	 }

     	 /**
     	 * Inserta un usuarios a la base de datos tomando los atributos de esta clase
     	 * @access private
     	 * @return bool
     	 */
    	 private function insertusuarios() {
        	 	 parent::conectar();
        	 	 $query = sprintf("INSERT INTO usuarios SET username = '%s',fecha_creacion = '%s',nombre = '%s',apellido = '%s',activo = '%s',password = '%s'",strip_tags(mysql_real_escape_string($this->_username)),strip_tags(mysql_real_escape_string($this->_fecha_creacion)),strip_tags(mysql_real_escape_string($this->_nombre)),strip_tags(mysql_real_escape_string($this->_apellido)),strip_tags(mysql_real_escape_string($this->_activo)),strip_tags(mysql_real_escape_string($this->_password)));
       	 	  $result = parent::ejecutarQueryWrite($query);
        	 	 parent::cerrar();
        	 	 if (!$result) {
            	 	 	 return false;
        	 	 } else {
            	 	 	 return true;
        	 	 }
    	 }
    	  /**
     	 * Elimina un usuarios de la base de datos
     	 * @access private
     	 * @param int $numerousuarios El id del usuarios a eliminar
     	 * @return bool
     	 */
    	 private function deleteusuarios($numerousuarios) {
        	 	 parent::conectar();
        	 	 $query = sprintf("DELETE FROM usuarios WHERE id = %d", $numerousuarios);
        	 	 $result = parent::ejecutarQueryWrite($query);
        	 	 parent::cerrar();
        	 	 if (!$result) {
            	 	 	 return false;
        	 	 } else {
            	 	 	 return true;
        	 	 }
     	 }

     	 /**
     	 * Actualiza los campos de un usuarios dentro de la base de datos
     	 * @access private
     	 * @param int $numerousuarios El id del usuarios a actualizar
     	 * @return bool
     	 */
    	 private function updateusuarios($numerousuarios) {
        	 	 parent::conectar();
        	 	 $query = sprintf("UPDATE usuarios SET username = '%s',fecha_creacion = '%s',nombre = '%s',apellido = '%s',activo = '%s',password = '%s' WHERE id = %d",strip_tags(mysql_real_escape_string($this->_username)),strip_tags(mysql_real_escape_string($this->_fecha_creacion)),strip_tags(mysql_real_escape_string($this->_nombre)),strip_tags(mysql_real_escape_string($this->_apellido)),strip_tags(mysql_real_escape_string($this->_activo)),strip_tags(mysql_real_escape_string($this->_password)), $numerousuarios);
        	 	 $result = parent::ejecutarQueryWrite($query);
        	 	 parent::cerrar();
        	 	 if (!$result) {
            	 	 	 return false;
        	 	 } else {
            	 	 	 return true;
        	 	 }
    	 }

}

?>
