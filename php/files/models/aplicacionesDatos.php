<?php

/**
 * Clase aplicacionesDatos
 *
 * Clase que representa a la tabla aplicaciones
 *
 * @version 1.0
 * @access public
 */
require_once "database.php";

class aplicacionesDatos extends database {
	 
    	 /**
	* Variable que contiene el id de aplicaciones
     	 */
   	  private $_id;
    	 /**
	* Variable que contiene el nombre de aplicaciones
     	 */
   	  private $_nombre;
    	 /**
	* Variable que contiene el fecha_creacion de aplicaciones
     	 */
   	  private $_fecha_creacion;
    	 /**
	* Variable que contiene el descripcion de aplicaciones
     	 */
   	  private $_descripcion;
    	 /**
	* Variable que contiene el nombre_producto de aplicaciones
     	 */
   	  private $_nombre_producto;
    	 /**
	* Variable que contiene el activo de aplicaciones
     	 */
   	  private $_activo;

    	 /**
     	 * Constructor.
     	 *
     	 */
    	 function aplicacionesDatos() {
        
    	 }

    	 /**
    	  * Funcion para obtener un aplicaciones por su id
     	 * @param $numeroaplicaciones El id del aplicaciones
     	 * @return void
     	 */
    	 function getaplicacionesById($numeroaplicaciones) {
        	 	 $this->idaplicaciones($numeroaplicaciones);
    	 }

     	 /**
     	 * Funcion para obtener todos los aplicacioneses
     	 * @return array
     	 */
    	 function getAllaplicaciones() {
        	 	 return $this->allaplicaciones();
    	 }

     	 /**
     	 * Funcion para agregar un aplicaciones a la base de datos
     	 * @return bool
     	 */
    	 function agregaraplicaciones() {
        	 	 return $this->insertaplicaciones();
    	 }
     	 /**
     	 * Funcion para eliminar un aplicaciones de la base de datos
     	 * @param $numeroaplicaciones El id del aplicaciones
     	 * @return bool
     	 */
    	 function eliminaraplicaciones($numeroaplicaciones) {
        	 	 return $this->deleteaplicaciones($numeroaplicaciones);
    	 }

     	 /**
     	 * Funcion para actualizar una aplicaciones dentro de la base de datos
     	 * @param int $numeroaplicaciones El id del aplicaciones
     	 * @return bool
     	 */
     	 function actualizaaplicaciones($numeroaplicaciones) {
        	 	 return $this->updateaplicaciones($numeroaplicaciones);
     	 }

    	 /**
     	 * Metodo set de la propiedad _id
     	 */
    	 function setid($id) {
       	 	  $this->_id = $id;
    	 }

    	 /**
     	 * Metodo get de la propiedad _id
     	 */
    	 function getid() {
        	 	 return $this->_id;
    	 }


    	 /**
     	 * Metodo set de la propiedad _nombre
     	 */
    	 function setnombre($nombre) {
       	 	  $this->_nombre = $nombre;
    	 }

    	 /**
     	 * Metodo get de la propiedad _nombre
     	 */
    	 function getnombre() {
        	 	 return $this->_nombre;
    	 }


    	 /**
     	 * Metodo set de la propiedad _fecha_creacion
     	 */
    	 function setfecha_creacion($fecha_creacion) {
       	 	  $this->_fecha_creacion = $fecha_creacion;
    	 }

    	 /**
     	 * Metodo get de la propiedad _fecha_creacion
     	 */
    	 function getfecha_creacion() {
        	 	 return $this->_fecha_creacion;
    	 }


    	 /**
     	 * Metodo set de la propiedad _descripcion
     	 */
    	 function setdescripcion($descripcion) {
       	 	  $this->_descripcion = $descripcion;
    	 }

    	 /**
     	 * Metodo get de la propiedad _descripcion
     	 */
    	 function getdescripcion() {
        	 	 return $this->_descripcion;
    	 }


    	 /**
     	 * Metodo set de la propiedad _nombre_producto
     	 */
    	 function setnombre_producto($nombre_producto) {
       	 	  $this->_nombre_producto = $nombre_producto;
    	 }

    	 /**
     	 * Metodo get de la propiedad _nombre_producto
     	 */
    	 function getnombre_producto() {
        	 	 return $this->_nombre_producto;
    	 }


    	 /**
     	 * Metodo set de la propiedad _activo
     	 */
    	 function setactivo($activo) {
       	 	  $this->_activo = $activo;
    	 }

    	 /**
     	 * Metodo get de la propiedad _activo
     	 */
    	 function getactivo() {
        	 	 return $this->_activo;
    	 }

    	 /**
     	 * Manda ejecutar el Select de los datos de la tabla aplicaciones, seleccionando un solo elemento y asignando los campos a los atributos.
     	 * @access private
     	 * @param int $numeroaplicaciones El id del registro
     	 * @return void
     	 */
    	 private function idaplicaciones($numeroaplicaciones) {
        	 	 parent::conectar();
        	 $query = "SELECT * FROM aplicaciones WHERE id = " . $numeroaplicaciones. "";
        	 	 $result = parent::ejecutarQuery($query);
        	 	 $this->_id = $result[0]['id'];
        	 	 $this->_nombre = $result[0]['nombre'];
        	 	 $this->_fecha_creacion = $result[0]['fecha_creacion'];
        	 	 $this->_descripcion = $result[0]['descripcion'];
        	 	 $this->_nombre_producto = $result[0]['nombre_producto'];
        	 	 $this->_activo = $result[0]['activo'];
        	 	 parent::cerrar();
    	 }

    	 /**
     	 * Manda ejecutar el Select de los datos de la tabla aplicaciones, seleccionando todos los registros.
     	 * @access private
     	 * @return resource
     	 */
    	 private function allaplicaciones() {
        	 	 parent::conectar();
        	 	 $query = "SELECT * FROM aplicaciones";
        	 	 $result = parent::ejecutarQuery($query);
        	 	 parent::cerrar();
        	 	 return $result;
    	 }

     	 /**
     	 * Inserta un aplicaciones a la base de datos tomando los atributos de esta clase
     	 * @access private
     	 * @return bool
     	 */
    	 private function insertaplicaciones() {
        	 	 parent::conectar();
        	 	 $query = sprintf("INSERT INTO aplicaciones SET nombre = '%s',fecha_creacion = '%s',descripcion = '%s',nombre_producto = '%s',activo = '%s'",strip_tags(mysql_real_escape_string($this->_nombre)),strip_tags(mysql_real_escape_string($this->_fecha_creacion)),strip_tags(mysql_real_escape_string($this->_descripcion)),strip_tags(mysql_real_escape_string($this->_nombre_producto)),strip_tags(mysql_real_escape_string($this->_activo)));
       	 	  $result = parent::ejecutarQueryWrite($query);
        	 	 parent::cerrar();
        	 	 if (!$result) {
            	 	 	 return false;
        	 	 } else {
            	 	 	 return true;
        	 	 }
    	 }
    	  /**
     	 * Elimina un aplicaciones de la base de datos
     	 * @access private
     	 * @param int $numeroaplicaciones El id del aplicaciones a eliminar
     	 * @return bool
     	 */
    	 private function deleteaplicaciones($numeroaplicaciones) {
        	 	 parent::conectar();
        	 	 $query = sprintf("DELETE FROM aplicaciones WHERE id = %d", $numeroaplicaciones);
        	 	 $result = parent::ejecutarQueryWrite($query);
        	 	 parent::cerrar();
        	 	 if (!$result) {
            	 	 	 return false;
        	 	 } else {
            	 	 	 return true;
        	 	 }
     	 }

     	 /**
     	 * Actualiza los campos de un aplicaciones dentro de la base de datos
     	 * @access private
     	 * @param int $numeroaplicaciones El id del aplicaciones a actualizar
     	 * @return bool
     	 */
    	 private function updateaplicaciones($numeroaplicaciones) {
        	 	 parent::conectar();
        	 	 $query = sprintf("UPDATE aplicaciones SET nombre = '%s',fecha_creacion = '%s',descripcion = '%s',nombre_producto = '%s',activo = '%s' WHERE id = %d",strip_tags(mysql_real_escape_string($this->_nombre)),strip_tags(mysql_real_escape_string($this->_fecha_creacion)),strip_tags(mysql_real_escape_string($this->_descripcion)),strip_tags(mysql_real_escape_string($this->_nombre_producto)),strip_tags(mysql_real_escape_string($this->_activo)), $numeroaplicaciones);
        	 	 $result = parent::ejecutarQueryWrite($query);
        	 	 parent::cerrar();
        	 	 if (!$result) {
            	 	 	 return false;
        	 	 } else {
            	 	 	 return true;
        	 	 }
    	 }

}

?>
