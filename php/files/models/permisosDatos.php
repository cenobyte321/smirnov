<?php

/**
 * Clase permisosDatos
 *
 * Clase que representa a la tabla permisos
 *
 * @version 1.0
 * @access public
 */
require_once "database.php";

class permisosDatos extends database {
	 
    	 /**
	* Variable que contiene el id de permisos
     	 */
   	  private $_id;
    	 /**
	* Variable que contiene el nombre de permisos
     	 */
   	  private $_nombre;
    	 /**
	* Variable que contiene el descripcion de permisos
     	 */
   	  private $_descripcion;
    	 /**
	* Variable que contiene el activo de permisos
     	 */
   	  private $_activo;

    	 /**
     	 * Constructor.
     	 *
     	 */
    	 function permisosDatos() {
        
    	 }

    	 /**
    	  * Funcion para obtener un permisos por su id
     	 * @param $numeropermisos El id del permisos
     	 * @return void
     	 */
    	 function getpermisosById($numeropermisos) {
        	 	 $this->idpermisos($numeropermisos);
    	 }

     	 /**
     	 * Funcion para obtener todos los permisoses
     	 * @return array
     	 */
    	 function getAllpermisos() {
        	 	 return $this->allpermisos();
    	 }

     	 /**
     	 * Funcion para agregar un permisos a la base de datos
     	 * @return bool
     	 */
    	 function agregarpermisos() {
        	 	 return $this->insertpermisos();
    	 }
     	 /**
     	 * Funcion para eliminar un permisos de la base de datos
     	 * @param $numeropermisos El id del permisos
     	 * @return bool
     	 */
    	 function eliminarpermisos($numeropermisos) {
        	 	 return $this->deletepermisos($numeropermisos);
    	 }

     	 /**
     	 * Funcion para actualizar una permisos dentro de la base de datos
     	 * @param int $numeropermisos El id del permisos
     	 * @return bool
     	 */
     	 function actualizapermisos($numeropermisos) {
        	 	 return $this->updatepermisos($numeropermisos);
     	 }

    	 /**
     	 * Metodo set de la propiedad _id
     	 */
    	 function setid($id) {
       	 	  $this->_id = $id;
    	 }

    	 /**
     	 * Metodo get de la propiedad _id
     	 */
    	 function getid() {
        	 	 return $this->_id;
    	 }


    	 /**
     	 * Metodo set de la propiedad _nombre
     	 */
    	 function setnombre($nombre) {
       	 	  $this->_nombre = $nombre;
    	 }

    	 /**
     	 * Metodo get de la propiedad _nombre
     	 */
    	 function getnombre() {
        	 	 return $this->_nombre;
    	 }


    	 /**
     	 * Metodo set de la propiedad _descripcion
     	 */
    	 function setdescripcion($descripcion) {
       	 	  $this->_descripcion = $descripcion;
    	 }

    	 /**
     	 * Metodo get de la propiedad _descripcion
     	 */
    	 function getdescripcion() {
        	 	 return $this->_descripcion;
    	 }


    	 /**
     	 * Metodo set de la propiedad _activo
     	 */
    	 function setactivo($activo) {
       	 	  $this->_activo = $activo;
    	 }

    	 /**
     	 * Metodo get de la propiedad _activo
     	 */
    	 function getactivo() {
        	 	 return $this->_activo;
    	 }

    	 /**
     	 * Manda ejecutar el Select de los datos de la tabla permisos, seleccionando un solo elemento y asignando los campos a los atributos.
     	 * @access private
     	 * @param int $numeropermisos El id del registro
     	 * @return void
     	 */
    	 private function idpermisos($numeropermisos) {
        	 	 parent::conectar();
        	 $query = "SELECT * FROM permisos WHERE id = " . $numeropermisos. "";
        	 	 $result = parent::ejecutarQuery($query);
        	 	 $this->_id = $result[0]['id'];
        	 	 $this->_nombre = $result[0]['nombre'];
        	 	 $this->_descripcion = $result[0]['descripcion'];
        	 	 $this->_activo = $result[0]['activo'];
        	 	 parent::cerrar();
    	 }

    	 /**
     	 * Manda ejecutar el Select de los datos de la tabla permisos, seleccionando todos los registros.
     	 * @access private
     	 * @return resource
     	 */
    	 private function allpermisos() {
        	 	 parent::conectar();
        	 	 $query = "SELECT * FROM permisos";
        	 	 $result = parent::ejecutarQuery($query);
        	 	 parent::cerrar();
        	 	 return $result;
    	 }

     	 /**
     	 * Inserta un permisos a la base de datos tomando los atributos de esta clase
     	 * @access private
     	 * @return bool
     	 */
    	 private function insertpermisos() {
        	 	 parent::conectar();
        	 	 $query = sprintf("INSERT INTO permisos SET nombre = '%s',descripcion = '%s',activo = '%s'",strip_tags(mysql_real_escape_string($this->_nombre)),strip_tags(mysql_real_escape_string($this->_descripcion)),strip_tags(mysql_real_escape_string($this->_activo)));
       	 	  $result = parent::ejecutarQueryWrite($query);
        	 	 parent::cerrar();
        	 	 if (!$result) {
            	 	 	 return false;
        	 	 } else {
            	 	 	 return true;
        	 	 }
    	 }
    	  /**
     	 * Elimina un permisos de la base de datos
     	 * @access private
     	 * @param int $numeropermisos El id del permisos a eliminar
     	 * @return bool
     	 */
    	 private function deletepermisos($numeropermisos) {
        	 	 parent::conectar();
        	 	 $query = sprintf("DELETE FROM permisos WHERE id = %d", $numeropermisos);
        	 	 $result = parent::ejecutarQueryWrite($query);
        	 	 parent::cerrar();
        	 	 if (!$result) {
            	 	 	 return false;
        	 	 } else {
            	 	 	 return true;
        	 	 }
     	 }

     	 /**
     	 * Actualiza los campos de un permisos dentro de la base de datos
     	 * @access private
     	 * @param int $numeropermisos El id del permisos a actualizar
     	 * @return bool
     	 */
    	 private function updatepermisos($numeropermisos) {
        	 	 parent::conectar();
        	 	 $query = sprintf("UPDATE permisos SET nombre = '%s',descripcion = '%s',activo = '%s' WHERE id = %d",strip_tags(mysql_real_escape_string($this->_nombre)),strip_tags(mysql_real_escape_string($this->_descripcion)),strip_tags(mysql_real_escape_string($this->_activo)), $numeropermisos);
        	 	 $result = parent::ejecutarQueryWrite($query);
        	 	 parent::cerrar();
        	 	 if (!$result) {
            	 	 	 return false;
        	 	 } else {
            	 	 	 return true;
        	 	 }
    	 }

}

?>
