<h2 class="title">
	usuarios
</h2>
<div class="content">
	<h3>Listado de usuarios</h3>
    <div style="color:red;">
		<strong><?php if(isset($notice)) echo $notice;  ?></strong>
	</div>
	<form action="index.php" method="post">
		<input type="hidden" name="p" value="usuarios" />
		<table border="1" style="font-size: 12px">
			<tr>
				<th>
					&nbsp;
				</th>
				<th>
					id
				</th>
				<th>
					username
				</th>
				<th>
					fecha_creacion
				</th>
				<th>
					nombre
				</th>
				<th>
					apellido
				</th>
				<th>
					activo
				</th>
				<th>
					password
				</th>
			</tr>
			<?php
				foreach ($usuarioss as $elemento) {
					echo '<tr>';
					echo '<td><input type="checkbox" name="selected[]" value="'.$elemento->getid().'" />'.'</td>';
					echo '<td>'.$elemento->getid().'</td>';
					echo '<td>'.$elemento->getusername().'</td>';
					echo '<td>'.$elemento->getfecha_creacion().'</td>';
					echo '<td>'.$elemento->getnombre().'</td>';
					echo '<td>'.$elemento->getapellido().'</td>';
					echo '<td>'.$elemento->getactivo().'</td>';
					echo '<td>'.$elemento->getpassword().'</td>';
					echo '</tr>';
				}
				?>
		</table>
		<br />
		<fieldset style="width:350px">
			<legend style="font-size:13px;">Operaciones</legend>
			<input type="submit" value="Agregar" name="a"  />
			<input type="submit" value="Eliminar seleccionados" name="a" />
			<input type="submit" value="Actualizar seleccionados" name="a" />
		</fieldset>
	</form>
</div>
