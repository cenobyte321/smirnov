<?php

require_once "models/permisosDatos.php";

class permisosLogica {


	function permisosLogica() {

	}

	/**
	* Colecciona todos los registros de permisos de la base de datos y muestra la vista con el listado de permisos
	* @access public
	* @param string $notice Mensaje opcional de advertencia a mostrar
	* @return void
	*/
	function listarpermisos($notice = NULL) {
		$permisosDat = new permisosDatos();
		$permisos =  $permisosDat->getAllpermisos();
		$permisoss = array();
		foreach ($permisos as $elemento) {
			$temp = new permisosDatos();
			$temp->getpermisosById($elemento['id']);
			array_push($permisoss, $temp);
		}
		include "views/rpermisosVista.php";
	}

	/**
	* Muestra una forma para insertar un permisos
	* @access public
	* @return void
	*/
	function agregar() {
		include "views/cpermisosVista.php";
	}

	/**
	* Inserta un permisos enviado por una forma. Al finalizar lista los registros o regresa un mensaje de error y muestra la forma para insertar 
	* @access public
	* @return void
	*/
	function crearpermisos() {
		$validacion = true;
		$permisosDat = new permisosDatos();
		$id = $_REQUEST['id'];
		$permisosDat->setid($id);
		$nombre = $_REQUEST['nombre'];
		$permisosDat->setnombre($nombre);
		$descripcion = $_REQUEST['descripcion'];
		$permisosDat->setdescripcion($descripcion);
		$activo = $_REQUEST['activo'];
		$permisosDat->setactivo($activo);
		$validacion = (filter_var($activo, FILTER_VALIDATE_BOOLEAN)) ? true: false;
		if($validacion){			if(!$permisosDat->agregarpermisos()) {
				$notice = "Error al agregar el nuevo registro";
				include "views/cpermisosVista.php";
				return;
			}

			$notice = "Registro agregado correctamente";
		}else{
			$notice = "Error de validacion";
		}
		$this->listarpermisos($notice);

	}

	/**
	* Elimina una serie de permisos seleccionados por una coleccion de checkboxes. Al final carga la vista que enlista todos los permisos
	* Si no se selecciona alguno se muestra un mensaje de error
	* @access public
	* @return void
	*/
	function eliminarseleccionados() {

		if (empty ($_REQUEST['selected'])) {
			$notice = "No se seleccionaron permisos a borrar";
			$this->listarpermisos($notice);
			return;
		}

		$permisosDat = new permisosDatos();
		$exito = true;


		foreach ($_REQUEST['selected'] as $id) {
			if(!$permisosDat->eliminarpermisos($id)) {
				$exito = false;
			}
		}
		$notice = ($exito == true) ? "Registros eliminados!": "Hubo un error al eliminar registros";
		$this->listarpermisos($notice);

	}

	/**
	* Muestra una forma con los permisos seleccionados (por una coleccion de checkboxes) a ser actualizados
	* Si no se selecciona permisos alguno se muestra un mensaje de error y se enlistan los permisos
	* @access public
	* @return void
	*/
	function actualizarseleccionados() {
		if (empty ($_REQUEST['selected'])) {
			$notice = "No se seleccionaron permisoses a actualizar";
			$this->listarpermisos($notice);
			return;
		}
		$permisoss = array();
		foreach ($_REQUEST['selected'] as $elemento) {
			$temp = new permisosDatos();
			$temp->getpermisosById($elemento);
			array_push($permisoss, $temp);
		}

		include "views/upermisosVista.php";


	}

	/**
	* Actualiza un arreglo de permisos de acuerdo con los datos enviados por una forma
	* Se regresa de nuevo al listado de permisos mostrando un mensaje de exito o de error
	* @access public
	* @return void
	*/
	function actualizarpermisos() {
		$exito = true;
		$permisosDat = new permisosDatos();
		for($i = 0; $i < count($_POST['id']); $i++) {
			$permisosDat->setid($_POST['id'][$i]);
			$permisosDat->setnombre($_POST['nombre'][$i]);
			$permisosDat->setdescripcion($_POST['descripcion'][$i]);
			$permisosDat->setactivo($_POST['activo'][$i]);
			if(!$permisosDat->actualizapermisos($_POST['id'][$i])){
$exito = false;
			}
		}

		$notice = ($exito == true) ? "Registros actualizados!": "Hubo un error al eliminar registros";
		$this->listarpermisos($notice);
	}
}
?>
