<?php

require_once "models/aplicacionesDatos.php";

class aplicacionesLogica {


	function aplicacionesLogica() {

	}

	/**
	* Colecciona todos los registros de aplicaciones de la base de datos y muestra la vista con el listado de aplicaciones
	* @access public
	* @param string $notice Mensaje opcional de advertencia a mostrar
	* @return void
	*/
	function listaraplicaciones($notice = NULL) {
		$aplicacionesDat = new aplicacionesDatos();
		$aplicaciones =  $aplicacionesDat->getAllaplicaciones();
		$aplicacioness = array();
		foreach ($aplicaciones as $elemento) {
			$temp = new aplicacionesDatos();
			$temp->getaplicacionesById($elemento['id']);
			array_push($aplicacioness, $temp);
		}
		include "views/raplicacionesVista.php";
	}

	/**
	* Muestra una forma para insertar un aplicaciones
	* @access public
	* @return void
	*/
	function agregar() {
		include "views/caplicacionesVista.php";
	}

	/**
	* Inserta un aplicaciones enviado por una forma. Al finalizar lista los registros o regresa un mensaje de error y muestra la forma para insertar 
	* @access public
	* @return void
	*/
	function crearaplicaciones() {
		$validacion = true;
		$aplicacionesDat = new aplicacionesDatos();
		$id = $_REQUEST['id'];
		$aplicacionesDat->setid($id);
		$nombre = $_REQUEST['nombre'];
		$aplicacionesDat->setnombre($nombre);
		$fecha_creacion = $_REQUEST['fecha_creacion'];
		$aplicacionesDat->setfecha_creacion($fecha_creacion);
		$descripcion = $_REQUEST['descripcion'];
		$aplicacionesDat->setdescripcion($descripcion);
		$nombre_producto = $_REQUEST['nombre_producto'];
		$aplicacionesDat->setnombre_producto($nombre_producto);
		$activo = $_REQUEST['activo'];
		$aplicacionesDat->setactivo($activo);
		$validacion = (filter_var($activo, FILTER_VALIDATE_BOOLEAN)) ? true: false;
		if($validacion){			if(!$aplicacionesDat->agregaraplicaciones()) {
				$notice = "Error al agregar el nuevo registro";
				include "views/caplicacionesVista.php";
				return;
			}

			$notice = "Registro agregado correctamente";
		}else{
			$notice = "Error de validacion";
		}
		$this->listaraplicaciones($notice);

	}

	/**
	* Elimina una serie de aplicaciones seleccionados por una coleccion de checkboxes. Al final carga la vista que enlista todos los aplicaciones
	* Si no se selecciona alguno se muestra un mensaje de error
	* @access public
	* @return void
	*/
	function eliminarseleccionados() {

		if (empty ($_REQUEST['selected'])) {
			$notice = "No se seleccionaron aplicaciones a borrar";
			$this->listaraplicaciones($notice);
			return;
		}

		$aplicacionesDat = new aplicacionesDatos();
		$exito = true;


		foreach ($_REQUEST['selected'] as $id) {
			if(!$aplicacionesDat->eliminaraplicaciones($id)) {
				$exito = false;
			}
		}
		$notice = ($exito == true) ? "Registros eliminados!": "Hubo un error al eliminar registros";
		$this->listaraplicaciones($notice);

	}

	/**
	* Muestra una forma con los aplicaciones seleccionados (por una coleccion de checkboxes) a ser actualizados
	* Si no se selecciona aplicaciones alguno se muestra un mensaje de error y se enlistan los aplicaciones
	* @access public
	* @return void
	*/
	function actualizarseleccionados() {
		if (empty ($_REQUEST['selected'])) {
			$notice = "No se seleccionaron aplicacioneses a actualizar";
			$this->listaraplicaciones($notice);
			return;
		}
		$aplicacioness = array();
		foreach ($_REQUEST['selected'] as $elemento) {
			$temp = new aplicacionesDatos();
			$temp->getaplicacionesById($elemento);
			array_push($aplicacioness, $temp);
		}

		include "views/uaplicacionesVista.php";


	}

	/**
	* Actualiza un arreglo de aplicaciones de acuerdo con los datos enviados por una forma
	* Se regresa de nuevo al listado de aplicaciones mostrando un mensaje de exito o de error
	* @access public
	* @return void
	*/
	function actualizaraplicaciones() {
		$exito = true;
		$aplicacionesDat = new aplicacionesDatos();
		for($i = 0; $i < count($_POST['id']); $i++) {
			$aplicacionesDat->setid($_POST['id'][$i]);
			$aplicacionesDat->setnombre($_POST['nombre'][$i]);
			$aplicacionesDat->setfecha_creacion($_POST['fecha_creacion'][$i]);
			$aplicacionesDat->setdescripcion($_POST['descripcion'][$i]);
			$aplicacionesDat->setnombre_producto($_POST['nombre_producto'][$i]);
			$aplicacionesDat->setactivo($_POST['activo'][$i]);
			if(!$aplicacionesDat->actualizaaplicaciones($_POST['id'][$i])){
$exito = false;
			}
		}

		$notice = ($exito == true) ? "Registros actualizados!": "Hubo un error al eliminar registros";
		$this->listaraplicaciones($notice);
	}
}
?>
