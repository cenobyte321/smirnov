<?php

require_once "models/usuariosDatos.php";

class usuariosLogica {


	function usuariosLogica() {

	}

	/**
	* Colecciona todos los registros de usuarios de la base de datos y muestra la vista con el listado de usuarios
	* @access public
	* @param string $notice Mensaje opcional de advertencia a mostrar
	* @return void
	*/
	function listarusuarios($notice = NULL) {
		$usuariosDat = new usuariosDatos();
		$usuarios =  $usuariosDat->getAllusuarios();
		$usuarioss = array();
		foreach ($usuarios as $elemento) {
			$temp = new usuariosDatos();
			$temp->getusuariosById($elemento['id']);
			array_push($usuarioss, $temp);
		}
		include "views/rusuariosVista.php";
	}

	/**
	* Muestra una forma para insertar un usuarios
	* @access public
	* @return void
	*/
	function agregar() {
		include "views/cusuariosVista.php";
	}

	/**
	* Inserta un usuarios enviado por una forma. Al finalizar lista los registros o regresa un mensaje de error y muestra la forma para insertar 
	* @access public
	* @return void
	*/
	function crearusuarios() {
		$validacion = true;
		$usuariosDat = new usuariosDatos();
		$id = $_REQUEST['id'];
		$usuariosDat->setid($id);
		$username = $_REQUEST['username'];
		$usuariosDat->setusername($username);
		$fecha_creacion = $_REQUEST['fecha_creacion'];
		$usuariosDat->setfecha_creacion($fecha_creacion);
		$nombre = $_REQUEST['nombre'];
		$usuariosDat->setnombre($nombre);
		$apellido = $_REQUEST['apellido'];
		$usuariosDat->setapellido($apellido);
		$activo = $_REQUEST['activo'];
		$usuariosDat->setactivo($activo);
		$validacion = (filter_var($activo, FILTER_VALIDATE_BOOLEAN)) ? true: false;
		$password = $_REQUEST['password'];
		$usuariosDat->setpassword($password);
		if($validacion){			if(!$usuariosDat->agregarusuarios()) {
				$notice = "Error al agregar el nuevo registro";
				include "views/cusuariosVista.php";
				return;
			}

			$notice = "Registro agregado correctamente";
		}else{
			$notice = "Error de validacion";
		}
		$this->listarusuarios($notice);

	}

	/**
	* Elimina una serie de usuarios seleccionados por una coleccion de checkboxes. Al final carga la vista que enlista todos los usuarios
	* Si no se selecciona alguno se muestra un mensaje de error
	* @access public
	* @return void
	*/
	function eliminarseleccionados() {

		if (empty ($_REQUEST['selected'])) {
			$notice = "No se seleccionaron usuarios a borrar";
			$this->listarusuarios($notice);
			return;
		}

		$usuariosDat = new usuariosDatos();
		$exito = true;


		foreach ($_REQUEST['selected'] as $id) {
			if(!$usuariosDat->eliminarusuarios($id)) {
				$exito = false;
			}
		}
		$notice = ($exito == true) ? "Registros eliminados!": "Hubo un error al eliminar registros";
		$this->listarusuarios($notice);

	}

	/**
	* Muestra una forma con los usuarios seleccionados (por una coleccion de checkboxes) a ser actualizados
	* Si no se selecciona usuarios alguno se muestra un mensaje de error y se enlistan los usuarios
	* @access public
	* @return void
	*/
	function actualizarseleccionados() {
		if (empty ($_REQUEST['selected'])) {
			$notice = "No se seleccionaron usuarioses a actualizar";
			$this->listarusuarios($notice);
			return;
		}
		$usuarioss = array();
		foreach ($_REQUEST['selected'] as $elemento) {
			$temp = new usuariosDatos();
			$temp->getusuariosById($elemento);
			array_push($usuarioss, $temp);
		}

		include "views/uusuariosVista.php";


	}

	/**
	* Actualiza un arreglo de usuarios de acuerdo con los datos enviados por una forma
	* Se regresa de nuevo al listado de usuarios mostrando un mensaje de exito o de error
	* @access public
	* @return void
	*/
	function actualizarusuarios() {
		$exito = true;
		$usuariosDat = new usuariosDatos();
		for($i = 0; $i < count($_POST['id']); $i++) {
			$usuariosDat->setid($_POST['id'][$i]);
			$usuariosDat->setusername($_POST['username'][$i]);
			$usuariosDat->setfecha_creacion($_POST['fecha_creacion'][$i]);
			$usuariosDat->setnombre($_POST['nombre'][$i]);
			$usuariosDat->setapellido($_POST['apellido'][$i]);
			$usuariosDat->setactivo($_POST['activo'][$i]);
			$usuariosDat->setpassword($_POST['password'][$i]);
			if(!$usuariosDat->actualizausuarios($_POST['id'][$i])){
$exito = false;
			}
		}

		$notice = ($exito == true) ? "Registros actualizados!": "Hubo un error al eliminar registros";
		$this->listarusuarios($notice);
	}
}
?>
