<?php

class CodeWriter {

    function crearArchivo($nombre, $contenidos, $carpeta) {
        $dir = '../files/' . $carpeta . '/';
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        $fileHandler = fopen($dir . $nombre, 'w') or die("No se pudo abrir el archivo");
        file_put_contents($dir . $nombre, $contenidos);
        fclose($fileHandler);
    }

}

?>
