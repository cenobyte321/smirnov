<?php

include_once '../models/Conexion.php';

class BaseDeDatos {

    private $_conexion;

    public function __construct() {
        $this->_conexion = new Conexion();
    }

    function obtenerBasesDeDatos() {
        $this->_conexion->conectar();
        $resultados = $this->_conexion->obtenerBDs();
        $this->_conexion->cerrar();
        return $resultados;
    }

    function obtenerTablas($baseDatos) {
        $this->_conexion->conectar();
        $this->_conexion->setDbNombre($baseDatos);
        $resultados = $this->_conexion->ejecutarQuery("SHOW TABLES FROM " . $baseDatos);
        foreach ($resultados as $key => $val) {
            $resultados[$key]['tabla'] = $resultados[$key]['Tables_in_' . $baseDatos];
            unset($resultados[$key]['Tables_in_' . $baseDatos]);
        }
        $this->_conexion->cerrar();
        return $resultados;
    }

    function obtenerFields($baseDatos, $tabla) {
        $this->_conexion->conectar();
        $this->_conexion->setDbNombre($baseDatos);
        $resultados = $this->_conexion->ejecutarQuery("SHOW COLUMNS FROM " . $tabla);
        $this->_conexion->cerrar();
        return $resultados;
    }

}

?>
