<?php

class Conexion {

    private $_dbHost = "localhost";
    private $_dbUsuario = "root";
    private $_dbPassword = "";
    private $_dbNombre;
    private $_db;

    function conectar() {
        $this->_db = mysql_connect($this->_dbHost, $this->_dbUsuario, $this->_dbPassword) or die('No se ha podido conectar con MySQL');
        if ($this->_dbNombre != NULL) {
            mysql_select_db($this->_dbNombre, $this->_db);
        }
    }

    function cerrar() {
        mysql_close($this->_db);
    }

    function obtenerBDs() {
        $dbList = mysql_list_dbs($this->_db);
        $basesDeDatos = array();
        while ($row = mysql_fetch_object($dbList)) {
            if ($row->Database != 'information_schema' && $row->Database != 'mysql' && $row->Database != 'performance_schema')
                array_push($basesDeDatos, $row->Database);
        }
        return $basesDeDatos;
    }

    function ejecutarQuery($query) {
        $result2 = array();
        $result = mysql_query($query, $this->_db) or die("No se pudo ejecutar el query:" . $query);
        while ($resultT = mysql_fetch_array($result))
            $result2[] = $resultT;
        return $result2;
    }

    function setDbNombre($dbNombre) {
        $this->_dbNombre = $dbNombre;
        mysql_select_db($this->_dbNombre, $this->_db);
    }

}

?>
