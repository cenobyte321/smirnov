
CREATE TABLE permisos (
                id INT AUTO_INCREMENT NOT NULL,
                nombre VARCHAR(120) NOT NULL,
                descripcion VARCHAR(120) NOT NULL,
                activo BOOLEAN DEFAULT true NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE usuarios (
                id INT AUTO_INCREMENT NOT NULL,
                username VARCHAR(120) NOT NULL,
                fecha_creacion DATE NOT NULL,
                nombre VARCHAR(120) NOT NULL,
                apellido VARCHAR(120) NOT NULL,
                activo BOOLEAN DEFAULT true NOT NULL,
                password VARCHAR(120) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE usuario_permisos (
                usuario_id INT NOT NULL,
                permiso_id INT NOT NULL,
                PRIMARY KEY (usuario_id, permiso_id)
);


CREATE TABLE aplicaciones (
                id INT AUTO_INCREMENT NOT NULL,
                nombre VARCHAR(120) NOT NULL,
                fecha_creacion DATE NOT NULL,
                descripcion VARCHAR(120) NOT NULL,
                nombre_producto VARCHAR(120) NOT NULL,
                activo BOOLEAN DEFAULT true NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE aplicacion_permisos (
                aplicacion_id INT NOT NULL,
                permiso_id INT NOT NULL,
                PRIMARY KEY (aplicacion_id, permiso_id)
);


CREATE TABLE aplicacion_usuarios (
                aplicacion_id INT NOT NULL,
                usuario_id INT NOT NULL,
                PRIMARY KEY (aplicacion_id, usuario_id)
);


ALTER TABLE usuario_permisos ADD CONSTRAINT permisos_usuario_permisos_fk
FOREIGN KEY (permiso_id)
REFERENCES permisos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE aplicacion_permisos ADD CONSTRAINT permisos_aplicacion_permisos_fk
FOREIGN KEY (permiso_id)
REFERENCES permisos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE usuario_permisos ADD CONSTRAINT usuarios_usuario_permisos_fk
FOREIGN KEY (usuario_id)
REFERENCES usuarios (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE aplicacion_usuarios ADD CONSTRAINT usuarios_aplicacion_usuarios_fk
FOREIGN KEY (usuario_id)
REFERENCES usuarios (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE aplicacion_usuarios ADD CONSTRAINT aplicaciones_aplicacion_usuarios_fk
FOREIGN KEY (aplicacion_id)
REFERENCES aplicaciones (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE aplicacion_permisos ADD CONSTRAINT aplicaciones_aplicacion_permisos_fk
FOREIGN KEY (aplicacion_id)
REFERENCES aplicaciones (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
