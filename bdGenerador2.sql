
CREATE TABLE tiendas (
                id INT AUTO_INCREMENT NOT NULL,
                direccion VARCHAR(120) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE empleados (
                id INT NOT NULL,
                nombrew VARCHAR(120) NOT NULL,
                apellido VARCHAR(120) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE empleado_tiendas (
                empleado_id INT NOT NULL,
                tienda_id INT NOT NULL,
                PRIMARY KEY (empleado_id, tienda_id)
);


CREATE TABLE productos (
                id INT AUTO_INCREMENT NOT NULL,
                nombre VARCHAR(120) NOT NULL,
                precio DOUBLE PRECISION NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE clientes (
                id INT AUTO_INCREMENT NOT NULL,
                nombre VARCHAR(120) NOT NULL,
                apellido VARCHAR(120) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ordenes (
                id INT NOT NULL,
                orden_fecha DATE NOT NULL,
                orden_cantidad INT NOT NULL,
                cliente_id INT NOT NULL,
                producto_id INT NOT NULL,
                empleado_id INT NOT NULL,
                PRIMARY KEY (id)
);


ALTER TABLE empleado_tiendas ADD CONSTRAINT tiendas_empleado_tiendas_fk
FOREIGN KEY (tienda_id)
REFERENCES tiendas (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE empleado_tiendas ADD CONSTRAINT empleados_empleado_tiendas_fk
FOREIGN KEY (empleado_id)
REFERENCES empleados (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ordenes ADD CONSTRAINT empleados_ordenes_fk
FOREIGN KEY (empleado_id)
REFERENCES empleados (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ordenes ADD CONSTRAINT productos_ordenes_fk
FOREIGN KEY (producto_id)
REFERENCES productos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ordenes ADD CONSTRAINT clientes_ordenes_fk
FOREIGN KEY (cliente_id)
REFERENCES clientes (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
